import Login from "./components/login/login"
import Register from "./components/register/register"
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Navbar from "./components/Navbar";      
import React, { createContext, useReducer } from 'react'
import "./App.css"
import Whiteboard from "./components/whiteboard2/whiteboard";
import Home from "./components/home/home";
import ForgotPassword from "./components/forgotPassword/forgotPassword";
import { initialstate, reducer } from "./reducer/UseReducer";
import Logout from "./components/logout/logout";
import "./components/ContainerCall"
import ContainerCall from "./components/ContainerCall";
import About from "./components/about/about";
import Select from "./components/select/select";

export const UserContext = createContext();

const Routing = () =>{
  return (
    <Switch>
      <Route exact path='/'><Home /></Route>
      <Route path='/about'><About /></Route>
      <Route path='/login'><Login /></Route>
      <Route path='/logout'><Logout /></Route>
      <Route path='/register'><Register /></Route>
      <Route path='/select'><Select /></Route>
      <Route path='/whiteboard'><Whiteboard /></Route>
      <Route path='/ContainerCall'><ContainerCall /></Route>
      <Route path='/forgotPassword'><ForgotPassword /></Route>
    </Switch>
  )
}

const App = () =>{

  const [state, dispatch] = useReducer(reducer, initialstate);

  return (
    
    <body className="app">
    <Router>
    <UserContext.Provider value={{state, dispatch}}>
    <Routing />
    </UserContext.Provider>
    </Router>
    </body>
  )
}

export default App