import React from 'react';
import '../App.css';
import { Button } from './Button';
import './HeroSection.css';
import img1 from "./about/learn.png"
import img2 from "./about/brainstorm.png"
import img3 from "./about/collaborate.png"
import Navbar from "./Navbar"

function HeroSection() {
  return (
    <div>
    <Navbar />
    <div className='hero-container'>
      <div className='hero-btns'>
        <Button
          className='btns'
          buttonStyle='btn--outline'
          buttonSize='btn--large'
        >
          GET STARTED
        </Button>
        </div>
    </div>
    </div>
  );
}

export default HeroSection;