import React, {useState, useEffect, useContext} from 'react'
import { Link } from 'react-router-dom'
import { Button } from './Button';
import "./Navbar.css";
import { UserContext } from "../App"
import img from "../dryEraseLogo.png"


const Navbar = () => {

    const {state, dispatch} = useContext(UserContext);
    
    const [click, setClick] = useState(false);
    const [button, setButton] = useState(true);

    const handleClick = () => setClick(!click);
    const closeMobileMenu = () => setClick(false);

    const showButton = () => {
        if (window.innerWidth <= 960) {
          setButton(false);
        } else {
          setButton(true);
        }
      };
    
      useEffect(() => {
        showButton();
      }, []);
    
      window.addEventListener('resize', showButton);


  const RenderMenu = () => {
    if (state){
      return (
        <>
        <ul className={click ? 'nav-menu active' : 'nav-menu'}>
        <li className="nav-item">
        <Link className="nav-links" to='/' id="section1" onClick={closeMobileMenu} smooth={true}> Home </Link> 
        </li>
        <li className="nav-item">
        <Link className="nav-links" to='/about' id="section2" onClick={closeMobileMenu} smooth={true}> About </Link> 
        </li>
        <li className="nav-item">
        <Link className="nav-links" to='/logout' onClick={closeMobileMenu} smooth={true}> Logout </Link> 
        </li>
        </ul>
        </>
      )
    }
    else {
      return(
      <>
      <ul className={click ? 'nav-menu active' : 'nav-menu'}>
      <div id="section1">
      <li className="nav-item">
      <Link className="nav-links" to='/' id="section1"  onClick={closeMobileMenu}> Home </Link> 
      </li>
      </div>
      <div id="section2">
      <li className="nav-item">
      <Link className="nav-links" to='/about' id="section2" onClick={closeMobileMenu}> About </Link> 
      </li>
      </div>
      <div id="section3">
      <li className="nav-item">
      <Link className="nav-links" to='/login' onClick={closeMobileMenu}> Login </Link> 
      </li>
      </div>
      <div>
      <li className="nav-item">
      <Link className="nav-links-mobile" to='/register' onClick={closeMobileMenu}> Sign Up </Link> 
      </li>
      </div>
      </ul>
      {button && <Button buttonStyle='btn--outline'> Sign Up </Button>}
      </>
    )}
    } 

  return (
    <>
    <nav className="navbar">
        <div className="navbar-container">
        
        <Link to="/" className="navbar-logo" onClick={closeMobileMenu}>
        <a className="navbar-brand" href="/">
        <img src={img} width="60" height="60" class="d-inline-block align-top" alt=""/></a>
            DRY ERASE
            </Link>
            <div className='menu-icon' onClick={handleClick}>
            <i className={click ? 'fas fa-times' : 'fas fa-bars'} />
            </div>
            <RenderMenu />
        </div>
    </nav>
    </>
  )
}
 
export default Navbar