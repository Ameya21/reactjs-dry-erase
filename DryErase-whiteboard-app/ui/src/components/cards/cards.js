import React from 'react';
import './cards.css';
import CardItem from './cardItem';
import img1 from "./learn.png"
import img2 from "./brainstorm.png"
import img3 from "./collaborate.png"

function Cards() {
  return (
    <div className='cards'>
      <div className='cards__container'>
        <h1> Check out these new features! </h1>
        <div className='cards__wrapper'>
          <ul className='cards__items'>
            <CardItem
              src={img1}
              text="Learn together!"
              path='/about'
            />
            <CardItem
              src={img2}
              text="Think together!"
              path='/about'
            />
            <CardItem
              src={img3}
              text="Work together!"
              path='/about'
            />
          </ul>
        </div>
      </div>
    </div>
  );
}

export default Cards;