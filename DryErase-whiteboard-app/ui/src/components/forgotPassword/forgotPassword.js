import React from 'react'
import { useState } from 'react'
const ForgotPassword = () => {

    const [ user, setUser] = useState({
        newPassword:"",
        reEnterNewPassword:"",
    })

    const handleChange = e => {
        const { name, value } = e.target
        setUser({
            ...user,
            [name]: value
        })
    }

  return (
    <div class="wrapper fadeInDown">
  <div id="formContent">
    <h2 class="active"> Reset your password </h2>

    <div class="fadeIn first">
    </div>

    <form >
      <input type="password" id="new" class="fadeIn second" name="newPassword" value={user.newPassword} onChange={handleChange} placeholder="Enter New Password" />
      <input type="password" id="newRe" class="fadeIn third" name="reEnterNewPassword" value={user.reEnterNewPassword} onChange={handleChange} placeholder="Confirm New Password" />
      <input type="submit" class="fadeIn fourth"/>
      <div><h2></h2></div>
    </form>
  </div>
</div>
  )
}

export default ForgotPassword