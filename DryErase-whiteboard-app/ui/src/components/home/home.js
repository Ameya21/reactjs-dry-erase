import React from 'react'
import Cards from '../cards/cards';
import Footer from '../footer/footer';
import "../../App.css"
import HeroSection from '../HeroSection'
import "./home.css"

function Home() {
  return (
    <>
    <HeroSection />
    <Cards />
    <Footer />
    </>
  )
}

export default Home;