import React, { useContext, useState } from "react"
import "./login.css"
import axios from "axios"
import { useHistory } from "react-router-dom"
import "../select/select"
import { UserContext } from "../../App"
import Navbar from "../Navbar"

const Login = () => {

    const { state, dispatch } = useContext(UserContext);

    const history = useHistory()

    const [user, setUser] = useState({
        email: "",
        password: "",
    })

    const [formErrors, setFormErrors] = useState({});

    const [records, setRecord] = useState([]);

    const handleChange = e => {
        const { name, value } = e.target
        setUser({
            ...user,
            [name]: value
        })
    }

    const handleSubmit = (e) => {
        e.preventDefault();
        login()
    }

    const validate = (values) => {
        const errors = {};
        const regex = /^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/i;
        if (!values.email) {
            errors.email = "Email is required *"
        } else if (!regex.test(values.email)) {
            errors.email = "This is an invalid email *"
        }
        if (!values.password) {
            errors.password = "Password is required *"
        } else if (values.password.length < 6) {
            errors.password = "Password must be atleast 6 characters *"
        }
        return errors
    }

    const login = () => {
        axios.get("http://localhost:3000/fetch")
            .then((res) => {
                var flag = false;
                for (var i = 0; i < res.data.length; i++) {
                    if ((res.data[i].email === user.email) && (res.data[i].password === user.password)) {
                        flag = 1;
                        break;
                    }
                }
                if (flag === 1){
                    console.log("match")
                    dispatch({ type: 'user', payload: true })
                    history.push("/select")
                }
                else{
                    alert("Invalid details \n Please try again.");
                    return;
                }
            })
    }

    return (
        <div>
        <Navbar />
        <div class="wrapper fadeInDown">
            <div id="formContent">
                <h2 class="active"> Sign In </h2>

                <div class="fadeIn first">
                    
                </div>

                <form onSubmit={handleSubmit}>
                    <i class="fa-solid fa-envelope fa-2x"></i>
                    <input type="text" id="login" class="fadeIn second" name="email" value={user.email} onChange={handleChange} placeholder="Email"/>
                    <p>{formErrors.email}</p>
                    <i class="fa-solid fa-lock fa-2x"></i>
                    <input type="password" id="password" class="fadeIn third" name="password" value={user.password} onChange={handleChange} placeholder="Password"/>
                    <p>{formErrors.password}</p>
                    <div><a class="underlineHover" href="/forgotPassword"> Forgot Password ?</a></div>
                    <input type="submit" class="fadeIn fourth"/>
                </form>
                <hr id="one"/>
                <h2>Don't have an account? <a class="underlineHover" href="/register"> Sign Up</a></h2>
            </div>
        </div>
        </div>
    )
}

export default Login