
import { UserContext } from "../../App"
import { useHistory } from 'react-router';
import { useContext } from 'react';
import "../login/login";

const Logout = () => {
    
    const history = useHistory()
    const {state, dispatch} = useContext(UserContext);
    dispatch({type:'user', payload:false})
    history.push("/login")
    return (
        <div>
        </div>
  )
}

export default Logout