import React, { useState, useContext} from "react"
import "./register.css"
import axios from "axios"
import { useHistory } from "react-router-dom"
import "../ContainerCall"
import { UserContext } from "../../App"
import "../select/select"
import Navbar from "../Navbar"

const Register = () => {

    const { state, dispatch } = useContext(UserContext);

    const history = useHistory()

    const [user, setUser] = useState({
        name: "",
        email: "",
        password: "",
        reEnterPassword: ""
    })

    const [formErrors, setFormErrors] = useState({});

    const handleChange = e => {
        const { name, value } = e.target
        setUser({
            ...user,
            [name]: value
        })
    }

    const handleSubmit = (e) => {
        e.preventDefault();
        setFormErrors(validate(user));
        console.log(formErrors)
        if (!formErrors.length) {
            register()
        }
        else {
            alert("error")
            return;
        }
    }

    const register = () => {
        const { name, email, password, reEnterPassword } = user
        if (name && email && password && (password === reEnterPassword)) {
            axios.get("http://localhost:3000/fetch").then((res) => {
                var flag = 0;
                for (var i = 0; i < res.data.length; i++) {
                    if (res.data[i].email === email) {
                        alert("User already exists")
                        flag = 1;
                        return;
                    }
                }
                if (flag == 0) {
                    axios.post("http://localhost:3000/register", user).then((res) => {
                        console.log(res.data)
                        dispatch({ type: 'user', payload: true })
                        history.push("/select")
                    })
                }
            })

        }
        else {
            alert("Invalid Input\nPlease check your details again.")
        }
    }

    const validate = (values) => {
        const errors = {};
        const regex = /^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/i;
        if (!values.name) {
            errors.name = "Name is required *"
        } else if (values.name.length < 1 || typeof values.name === 'number') {
            errors.name = "Invalid Name *"
        }
        if (!values.email) {
            errors.email = "Email is required *"
        } else if (!regex.test(values.email)) {
            errors.email = "This is an invalid email *"
        }
        if (!values.password) {
            errors.password = "Password is required *"
        } else if (values.password.length < 6) {
            errors.password = "Password must be atleast 6 characters *"
        }
        return errors
    }

    return (
        <>
        <Navbar />
        <div id="register" class="wrapper fadeInDown">
            <div id="formContent">
                <h2 class="active"> Sign Up </h2>
                <form onSubmit={handleSubmit}>
                    <input type="text" id="name" class="fadeIn second" name="name" value={user.name} onChange={handleChange} placeholder="Your Name" />
                    <p>{formErrors.name}</p>
                    <input type="text" id="login" class="fadeIn second" name="email" value={user.email} onChange={handleChange} placeholder="Your Email" />
                    <p>{formErrors.email}</p>
                    <input type="password" id="password" class="fadeIn second" name="password" value={user.password} onChange={handleChange} placeholder="Your Password" />
                    <p>{formErrors.password}</p>
                    <input type="password" id="repassword" class="fadeIn third" name="reEnterPassword" value={user.reEnterPassword} onChange={handleChange} placeholder="Confirm Password" />
                    <p>{formErrors.reEnterPassword}</p>
                    <input type="submit" class="fadeIn fourth" />
                </form>
                <div>
                    <h2 className="forgot-password text-right">
                        Don't have an account? <a class="underlineHover" href="/login"> Sign In</a>
                    </h2>
                </div>
            </div>
        </div>
        </>
    )
    }

    export default Register