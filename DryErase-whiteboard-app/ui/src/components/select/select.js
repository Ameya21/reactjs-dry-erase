import React from 'react'
import '../ContainerCall'
import "../whiteboard2/whiteboard"
import { useHistory } from 'react-router-dom'
import Navbar from '../Navbar'
import "./select.css"


const Select = () =>{
    const history = useHistory()

    const select = () => {
      history.push("/ContainerCall")
    }

    const select2 = () => {
      history.push("/whiteboard")
    }

  return (
    <>
    <Navbar />
    <div>
    <h1 id="selcth">Welcome to Dry Erase!</h1>
    <h1 id="selcth">How do you want to use the whiteboard?</h1>
    <button id="individual" onClick={select2}></button>
    <button id="collaborate" onClick={select}></button>
    </div>
    </>
    )
}

export default Select