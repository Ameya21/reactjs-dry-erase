import React from 'react';
import ReactDOM from 'react-dom';
import './whiteboard-index.css';
import Whiteboard from './whiteboard';
import 'bootstrap/dist/css/bootstrap.min.css';

ReactDOM.render(
  <React.StrictMode>
    <Whiteboard />
  </React.StrictMode>,
  document.getElementById('root')
);
