**PROJECT : DRY ERASE**

**Team Members:**
E. Ameya    - 19WH1A04A2 (ECE)\
M. Akshita - 19WH1A0567 (CSE)\
P. Harshini - 19WH1A05A3 (CSE)\
M. Sriya    - 19WH1A1242 (IT)\
M. Sanjana  - 19WH1A0236 (EEE)

**About:** A whiteboard web application, which enables people to brainstorm together, share ideas and work collaboratively.

**Technology:** MERN Stack and Socket io
