import React,{Component} from 'react';

class Search extends Component{
    state={
        searchText:'',
        apiUrl:'https://pixabay.com/api',
        apiKey:'26065730-7b25c7d427153fce477d70f74',
        images:[]
    };
    onTextChange=e=>{
        this.setState({[e.target.name]:e.target.value});
    }
    render(){
        return(
            <div>
            <input type="text"
             style={{backgroundColor:'black',
             color:'white',
             marginLeft:570,
             marginTop:100,
             paddingTop:20,
             paddingLeft:70,
             fontsize:30,
             borderTopStyle:"hidden",
             borderRightStyle:"hidden",
             borderLeftStyle:"hidden",
             outline:"none",
             borderBottomStyle:"groove",
            }}
            placeholder="Search for images"
            name = "searchText"
            value={this.state.searchText}
            onChange={this.onTextChange}
            
            />
            </div>
        )
    }
}


export default Search;