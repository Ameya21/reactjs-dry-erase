import React, { useLayoutEffect, useState } from 'react';
import rough from 'roughjs/bundled/rough.esm';

const generator = rough.generator();

function createElement(x1, y1, x2, y2) {
  const roughElement = generator.line(x1, y1, x2, y2);
  return { x1, y1, x2, y2, roughElement };
}

const Drawingtool = () => {

  const [elements, setElements] = useState([]);

  const [drawing, setDrawing] = useState(false);

  useLayoutEffect(() => {
    const canvas = document.getElementById('canvas');
    const context = canvas.getContext('2d');

    context.clearRect(0, 0, canvas.width, canvas.height);

    const roughCanvas = rough.canvas(canvas);

    elements.forEach((ele) => roughCanvas.draw(ele.roughElement));
  }, [elements]);

  const startDraw = (event) => {
    setDrawing(true);
    const { clientX, clientY } = event;
    const newElement = createElement(clientX, clientY, clientX, clientY);
    setElements((state) => [...state, newElement]);
  };
  const finishDraw = () => {
    setDrawing(false);
  };
  const draw = (event) => {
    if (!drawing) return;

    const { clientX, clientY } = event;
    console.log(clientX, clientY);
    const index = elements.length - 1;
    const { x1, y1 } = elements[index];
    const updatedElement = createElement(x1, y1, clientX, clientY);

    const copyElement = [...elements];
    copyElement[index] = updatedElement;
    setElements(copyElement);
  };
  return (
    <canvas
      id='canvas'
      width={window.innerWidth}
      height={window.innerWidth}
      onMouseDown={startDraw}
      onMouseUp={finishDraw}
      onMouseMove={draw}
    >

    </canvas>
  );
};
export default Drawingtool;