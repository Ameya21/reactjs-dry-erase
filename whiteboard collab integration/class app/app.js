import React, { useEffect, useState } from "react";
import "./App.css";
import Swatch from "./components/Swatch";
import socketIOClient from "socket.io-client";
import rough from "roughjs/bundled/rough.esm";
import {
  createElement,
  adjustElementCoordinates,
  cursorForPosition,
  resizedCoordinates,
  midPointBtw,
  getElementAtPosition,
} from "./components/element";
class App extends React.Component {
    constructor(props){
        super(props);
  const ENDPOINT = "http://127.0.0.1:4001";
  this.state={
      points: []
  };
  this.state={
      path : []
  };
  this.state={
      isDrawing : false
  };
  this.state={
      elements : []
  };
  this.state={
      action : "none"
  };
  this.state={
      toolType : "pencil"
  };
  this.state={
      selectedElement : null
  };
  this.state={
      colorWidth : {
        hex: "#000",
        hsv: {},
        rgb: {},
      }
  };
  this.state={
      width : 1
  };
  this.state={
      shapeWidth : 1
  };
  this.state={
      popped : false
  };
  this.state={
      response : ""
  };

    

  useEffect(() => {
    const canvas = document.getElementById("canvas");
    const context = canvas.getContext("2d");
    context.lineCap = "round";
    context.lineJoin = "round";

    context.save();
    const socket = socketIOClient(ENDPOINT);
    socket.on("canvas-data", data => {
        this.setState({response:data});
    });
    const drawpath = () => {
      this.state.path.forEach((stroke, index) => {
        context.beginPath();

        stroke.forEach((point, i) => {
          context.strokeStyle = point.newColour;
          context.lineWidth = point.newLinewidth;

          var midPoint = midPointBtw(point.clientX, point.clientY);

          context.quadraticCurveTo(
            point.clientX,
            point.clientY,
            midPoint.x,
            midPoint.y
          );
          context.lineTo(point.clientX, point.clientY);
          context.stroke();
        });
        context.closePath();
        context.save();
      });
    };
    const tool = this.state.toolType;
    const pop = this.state.popped;

    if (tool === "eraser" && pop === true) {
      context.clearRect(0, 0, canvas.width, canvas.height);
      this.setState({popped:false});
    }

    const roughCanvas = rough.canvas(canvas);
    const pathconst = this.state.path;

    if (pathconst !== undefined) drawpath();

    context.lineWidth = this.state.shapeWidth;

    this.state.elements.forEach(({ roughElement }) => {
      context.globalAlpha = "1";
      //console.log(roughElement);
      context.strokeStyle = roughElement.options.stroke;
      roughCanvas.draw(roughElement);
    });
    return () => {
      context.clearRect(0, 0, canvas.width, canvas.height);
    };
  }, [this.state.popped, this.state.elements, this.state.path, this.state.width]);

  const updateElement = (
    index,
    x1,
    y1,
    x2,
    y2,
    toolType,
    strokeWidth,
    strokeColor
  ) => {
    const updatedElement = createElement(
      index,
      x1,
      y1,
      x2,
      y2,
      toolType,
      strokeWidth,
      strokeColor
    );
    const elementsCopy = [...this.state.elements];
    elementsCopy[index] = updatedElement;
    this.setState({elements:elementsCopy});
  };

  const checkPresent = (clientX, clientY) => {
    if (this.state.path === undefined) return;
    var newPath = this.state.path;
    this.state.path.forEach((stroke, index) => {
      stroke.forEach((point, i) => {
        if (
          clientY < point.clientY + 10 &&
          clientY > point.clientY - 10 &&
          clientX < point.clientX + 10 &&
          clientX > point.clientX - 10
        ) {
          //console.log("Popped");
          newPath.splice(index, 1);
          this.setState({popped:true});
          this.setState({path:newPath});
          return;
        }
      });
    });
    const newElements = this.state.elements;
    newElements.forEach((ele, index) => {
      if (
        clientX >= ele.x1 &&
        clientX <= ele.x2 &&
        clientY >= ele.y1 &&
        clientY <= ele.y2
      ) {
        console.log("Popped....");
        newElements.splice(index, 1);
        this.setState({popped:true});
        this.setState({elements:newElements});
      }
    });
  };

  function handleMouseDown(e){
    const { clientX, clientY } = e;
    const canvas = document.getElementById("canvas");
    const context = canvas.getContext("2d");

    if (this.state.toolType === "selection") {
      const element = getElementAtPosition(clientX, clientY, this.state.elements);
      if (element) {
        const offsetX = clientX - element.x1;
        const offsetY = clientY - element.y1;
        this.setState({selectedElement:{ ...element, offsetX, offsetY }});
        if (element.position === "inside") {
            this.setState({action:"moving"});
        } else {
            this.setState({action:"resize"});
        }
      }
    } else if (this.state.toolType === "eraser") {
        this.setState({action:"erasing"});

      checkPresent(clientX, clientY);
    } else {
      const id = this.state.elements.length;
      if (this.state.toolType === "pencil" || this.state.toolType === "brush") {
        this.setState({action:"sketching"});
        this.setState({isDrawing:true});

        const newColour = this.state.colorWidth.hex;
        const newLinewidth = this.state.width;
        const transparency = this.state.toolType === "brush" ? "0.1" : "1.0";
        const newEle = {
          clientX,
          clientY,
          newColour,
          newLinewidth,
          transparency,
        };
        this.setState({points:(state) => [...state, newEle]});

        context.strokeStyle = newColour;
        context.lineWidth = newLinewidth;
        context.lineCap = 5;
        context.moveTo(clientX, clientY);
        context.beginPath();
      } else {
        this.setState({action:"drawing"});
        const newColour = this.state.colorWidth.hex;
        const newWidth = this.state.shapeWidth;
        const element = createElement(
          id,
          clientX,
          clientY,
          clientX,
          clientY,
          this.state.toolType,
          newWidth,
          newColour
        );
        this.setState({elements:(prevState) => [...prevState, element]});
        this.setState({selectedElement: element});
      }
    }
  };

  function handleMouseMove(e){
    const canvas = document.getElementById("canvas");
    const context = canvas.getContext("2d");
    const { clientX, clientY } = e;
    if (this.state.toolType === "selection") {
      const element = getElementAtPosition(clientX, clientY, this.state.elements);
      e.target.style.cursor = element
        ? cursorForPosition(element.position)
        : "default";
    }
    if (this.state.action === "erasing") {
      checkPresent(clientX, clientY);
    }
    if (this.state.action === "sketching") {
      if (!this.state.isDrawing) return;
      const colour = this.state.points[this.state.points.length - 1].newColour;
      const linewidth = this.state.points[this.state.points.length - 1].newLinewidth;
      const transparency = this.state.points[this.state.points.length - 1].transparency;
      const newEle = { clientX, clientY, colour, linewidth, transparency };
      this.setState({points: (state) => [...state, newEle]});
      var midPoint = midPointBtw(clientX, clientY);
      context.quadraticCurveTo(clientX, clientY, midPoint.x, midPoint.y);
      context.lineTo(clientX, clientY);
      context.stroke();
    } else if (this.state.action === "drawing") {
      const index = this.state.elements.length - 1;
      const { x1, y1 } = this.state.elements[index];
      this.state.elements[index].strokeColor = this.state.colorWidth.hex;
      this.state.elements[index].strokeWidth = this.state.shapeWidth;
      updateElement(
        index,
        x1,
        y1,
        clientX,
        clientY,
        this.state.toolType,
        this.state.shapeWidth,
        this.state.colorWidth.hex
      );
    } else if (this.state.action === "moving") {
      const {
        id,
        x1,
        x2,
        y1,
        y2,
        type,
        offsetX,
        offsetY,
        shapeWidth,
        strokeColor,
      } = this.state.selectedElement;
      const offsetWidth = x2 - x1;
      const offsetHeight = y2 - y1;
      const newX = clientX - offsetX;
      const newY = clientY - offsetY;
      updateElement(
        id,
        newX,
        newY,
        newX + offsetWidth,
        newY + offsetHeight,
        type,
        shapeWidth,
        strokeColor
      );
    } else if (this.state.action === "resize") {
      const { id, type, position, ...coordinates } = this.state.selectedElement;
      const { x1, y1, x2, y2 } = resizedCoordinates(
        clientX,
        clientY,
        position,
        coordinates
      );
      updateElement(id, x1, y1, x2, y2, type, this.state.shapeWidth, this.state.colorWidth.hex);
    }
  };
  function handleMouseUp(){
    if (this.state.action === "resize") {
      const index = this.state.selectedElement.id;
      const { id, type, strokeWidth, strokeColor } = this.state.elements[index];
      const { x1, y1, x2, y2 } = adjustElementCoordinates(this.state.elements[index]);
      updateElement(id, x1, y1, x2, y2, type, strokeWidth, strokeColor);
    } else if (this.state.action === "drawing") {
      const index = this.state.selectedElement.id;
      const { id, type, strokeWidth } = this.state.elements[index];
      const { x1, y1, x2, y2 } = adjustElementCoordinates(this.state.elements[index]);
      updateElement(id, x1, y1, x2, y2, type, strokeWidth, this.state.colorWidth.hex);
    } else if (this.state.action === "sketching") {
      const canvas = document.getElementById("canvas");
      const context = canvas.getContext("2d");
      context.closePath();
      const element = this.state.points;
      this.setState({points: []});
      this.setState({path: (prevState) => [...prevState, element]});
      this.setState({ isDrawing : false});
    }
    this.setState({action : "none"});
  };
}
render(){
  return (
    <div>
      <Swatch
        toolType={this.toolType}
        setToolType={this.state.toolType}
        width={this.width}
        setWidth={this.state.width}
        setElements={this.state.elements}
        setColorWidth={this.state.colorWidth}
        setPath={this.state.path}
        colorWidth={this.colorWidth}
        setShapeWidth={this.state.shapeWidth}
      />
      <canvas
        id="canvas"
        className="App"
        width={window.innerWidth}
        height={window.innerHeight}
        onMouseDown={this.handleMouseDown}
        onMouseMove={this.handleMouseMove}
        onMouseUp={this.handleMouseUp}
        onTouchStart={(e) => {
          var touch = e.touches[0];
          this.handleMouseDown({ clientX: touch.clientX, clientY: touch.clientY });
        }}
        onTouchMove={(e) => {
          var touch = e.touches[0];
          this.handleMouseMove({ clientX: touch.clientX, clientY: touch.clientY });
        }}
        onTouchEnd={this.handleMouseUp}
      >
      </canvas>
    </div>
  );
}
}


export default App;