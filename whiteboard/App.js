import './App.css';
import './index.css';
import { render } from "react-dom";
import React from 'react';

import Board from './Board.js';
function App(){
render(
  <React.StrictMode>
    <Board />
  </React.StrictMode>,
  document.getElementById('root')
);
}

export default App;
